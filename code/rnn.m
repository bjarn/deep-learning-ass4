classdef rnn
    properties
        K; m;
        param;
        grad;
        moment;
        rawmoment;
        a; h; o; P;
    end
    methods
        %Constructor
        function obj = rnn(m, K, sig)
            if nargin == 1
                obj.param = m;
                obj.K = size(obj.param.V,1);
                obj.m = size(obj.param.V,2);
            elseif nargin > 1
                obj.param.W = randn(m, m)*sig;
                obj.grad.W = zeros(m, m);
                obj.moment.W = zeros(m, m);
                obj.rawmoment.W = zeros(m, m);
                
                obj.param.U = randn(m, K)*sig;
                obj.grad.U = zeros(m, K);
                obj.moment.U = zeros(m, K);
                obj.rawmoment.U = zeros(m, K);
                
                obj.param.b = zeros(m,1);
                obj.grad.b = zeros(m,1);
                obj.moment.b = zeros(m, 1);
                obj.rawmoment.b = zeros(m, 1);
                
                obj.param.V = randn(K, m)*sig;
                obj.grad.V = zeros(K, m);
                obj.moment.V = zeros(K, m);
                obj.rawmoment.V = zeros(K, m);
                
                obj.param.c = zeros(K,1);
                obj.grad.c = zeros(K,1);
                obj.moment.c = zeros(K, 1);
                obj.rawmoment.c = zeros(K, 1);
                
                obj.K = K;
                obj.m = m;
            end
        end
        
        %Compute cost
        function cost = computeCost(obj, Y)
            cost = -sum(log(diag(Y'*obj.P)));
        end
        
        %Calculates the difference between 2 gradient matrices. Used to confirm
        %analytical gradients
        function error = relErrOfGrads(obj, grad_num)
            for f = fieldnames(grad_num)'
                error.(f{1}) = sum(sum(abs(obj.grad.(f{1}) - grad_num.(f{1}))))%/numel(grad_num.(f{1}));
            end
        end
        
        %Forward Pass
        function obj = forwardPass(obj, h0, X)
            n = size(X, 2);
            obj.a = zeros(obj.m,n);
            %Will contain h0 at first place
            obj.h = zeros(obj.m,n+1);
            obj.h(:,1) = h0;
            obj.o = zeros(obj.K,n);
            obj.P = zeros(obj.K,n);
            for t = 1:n
                obj.a(:,t) = obj.param.W*obj.h(:,t) + obj.param.U*X(:,t) + obj.param.b;
                obj.h(:,t+1) = tanh(obj.a(:,t));
                obj.o(:,t) = obj.param.V*obj.h(:,t+1) + obj.param.c;
                obj.P(:,t) = softmax(obj.o(:,t));
            end
        end
        %Backwards pass
        function obj = backwardPass(obj, X, Y)
            obj.grad.W = zeros(obj.m, obj.m);
            obj.grad.U = zeros(obj.m, obj.K);
            obj.grad.b = zeros(obj.m,1);
            obj.grad.V = zeros(obj.K, obj.m);             
            obj.grad.c = zeros(obj.K,1);
            for t = size(Y,2):-1:1
                dp = -Y(:,t)'/(Y(:,t)'*obj.P(:,t));
                do = dp * (diag(obj.P(:,t))-obj.P(:,t)*obj.P(:,t)');
                
                obj.grad.c = obj.grad.c + do';
                obj.grad.V = obj.grad.V + do' * obj.h(:,t + 1)';
                
                if t == size(Y,2)
                    dh = do * obj.param.V;
                else    
                    dh = do * obj.param.V + prevda * obj.param.W;
                end
                da = dh * diag (1-tanh(obj.a(:,t)).^2);
                prevda = da;

                obj.grad.W = obj.grad.W + da' * obj.h(:,t)';
                obj.grad.U = obj.grad.U + da' * X(:,t)';
                obj.grad.b = obj.grad.b + da';
            end
            %Clip gradients
            for f = fieldnames(obj.grad)'
                obj.grad.(f{1}) = max(min(obj.grad.(f{1}), 5), -5);
            end
        end
        %Adagrad update
        function obj = update(obj, eta)
            EPS = 1e-8;
            for f = fieldnames(obj.param)'
                obj.moment.(f{1}) = obj.moment.(f{1}) + obj.grad.(f{1}).^2;
                obj.param.(f{1}) =  obj.param.(f{1}) -  (eta * obj.grad.(f{1})) ./ sqrt(obj.moment.(f{1}) + EPS);
             end
        end
        %RMS update
        function obj = updateRMS(obj, eta, gamma)
            EPS = 1e-8;
            for f = fieldnames(obj.param)'
                obj.moment.(f{1}) = gamma*obj.moment.(f{1}) + (1-gamma)*obj.grad.(f{1}).^2;
                obj.param.(f{1}) =  obj.param.(f{1}) -  (eta * obj.grad.(f{1}))./sqrt(obj.moment.(f{1}) + EPS);
             end
        end
        %ADAM update
        function obj = updateADAM(obj, eta, beta1, beta2)
            EPS = 1e-8;
            for f = fieldnames(obj.param)'
                obj.moment.(f{1}) = beta1*obj.moment.(f{1}) + (1-beta1)*obj.grad.(f{1});
                obj.rawmoment.(f{1}) = beta2*obj.rawmoment.(f{1}) + (1-beta2)*(obj.grad.(f{1}).^2);
                obj.param.(f{1}) =  obj.param.(f{1}) -  (eta * obj.moment.(f{1}))./(sqrt(obj.rawmoment.(f{1}))+EPS);
             end
        end
        %Synthesize sequences
        function Y = synthSeq(obj, h0, x, n)
            tempa = zeros(obj.m,n);
            %Will contain h0 at first place
            temph = zeros(obj.m,n+1);
            temph(:,1) = h0;
            tempo = zeros(obj.K,n);
            tempp = zeros(obj.K,n);
            Y = zeros(obj.K,n);
            for t = 1:n
                Y(:,t) = x;
                tempa(:,t) = obj.param.W*temph(:,t) + obj.param.U*x + diag(obj.param.b)*ones(size(obj.param.U,1),1);
                temph(:,t+1) = tanh(tempa(:,t));
                tempo(:,t) = obj.param.V*temph(:,t+1) + obj.param.c;
                tempp(:,t) = softmax(tempo(:,t));
                cp = cumsum(tempp(:,t));
                a = rand;
                ixs = find(cp-a >0);
                ii = ixs(1);
                x = [1:obj.K]'==ii;
            end
        end
    end
end


