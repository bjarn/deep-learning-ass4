%%START OF MAIN-SCRIPT
%%
%Load data
format long;
book_fname = 'data/goblet_book.txt';
fid = fopen(book_fname,'r');
book_data = fscanf(fid,'%c');
fclose(fid);

%Process data
book_chars = unique(char(book_data));

chars =num2cell(char(book_chars));
indexes = 1:size(book_chars,2);
char_to_ind = containers.Map(chars,indexes);
ind_to_char = containers.Map(indexes,chars);
%%
%Initialize params for RNN
%Hidden nodes
m=100;
seq_length=25;
K=size(book_chars,2);
sig = 0.01;
%%
%Test synthesizing text
rnnet = rnn(m,K,sig);
synth = rnnet.synthSeq(zeros(m,1), full(ind2vec(20,K)), 200);
cell2mat(values(ind_to_char, num2cell(vec2ind(synth))))
%%
%Generate 1000 char sequence
synth = rnnet.synthSeq(hprev, full(ind2vec(cell2mat(values(char_to_ind, num2cell(char(book_chars(:, 32))))),84)), 1000);
text=values(ind_to_char, num2cell(vec2ind(synth)));
cell2mat(text)
%%
%Check gradients
X_chars = book_data(1:seq_length);
Y_chars = book_data(2:seq_length+1);
X = full(ind2vec(cell2mat(values(char_to_ind, num2cell(char(X_chars)))),K));
Y = full(ind2vec(cell2mat(values(char_to_ind, num2cell(char(Y_chars)))),K));
rnnet = rnnet.forwardPass(zeros(m,1), X);
cost = rnnet.computeCost(Y);
rnnet = rnnet.backwardPass(X, Y);
num_grads = ComputeGradsNum(X, Y, rnnet.param, 1e-4);
relErr = rnnet.relErrOfGrads(num_grads)
%%
%Train network

n_epoch = 10;
result.loss = {};
result.text = {};
%For Adagrad
% eta = 0.042080200187300;

%For RMSprop
% eta = 0.003809129186068;
% gamma = 0.989664439408021;

%For ADAM
eta = 0.001;
beta1 = 0.9;
beta2 = 0.999;

%kinda cheating
min_loss = 45;
rnnet = rnn(m,K,sig);
prev_loss = 0;
for epoch = 1:n_epoch
    hprev = zeros(m,1);
    disp(['Epoch: ' num2str(epoch) ]);
    for e = 1:seq_length:size(book_data,2)
        if e>length(book_data)-seq_length
            break;
        end
        
        %New batch
        X_chars = book_data(e:e+seq_length-1);
        Y_chars = book_data(e+1:e+seq_length);
        X = full(ind2vec(cell2mat(values(char_to_ind, num2cell(char(X_chars)))),K));
        Y = full(ind2vec(cell2mat(values(char_to_ind, num2cell(char(Y_chars)))),K));
        
        %Train
        rnnet = rnnet.forwardPass(zeros(m,1), X);
        rnnet = rnnet.backwardPass(X, Y);
%         %Adagrad
%         rnnet = rnnet.update(eta);
%         %RMS
%         rnnet = rnnet.updateRMS(eta, gamma);
%         %ADAM
        rnnet = rnnet.updateADAM(eta, beta1, beta2);
        
        %Compute smooth loss
        loss = rnnet.computeCost(Y);
        if (e == 1) && (epoch == 1)
            smooth_loss = loss;
        end
        smooth_loss = .999* smooth_loss + .001 * loss;
        
        %Save loss and synthesize text of 200 chars
        iter = ceil((size(book_data,2)*(epoch-1)+e)/seq_length);
        if(mod(iter, 1000) == 0)
            result.loss{end+1} = [epoch, iter, smooth_loss];
            disp(['Iteration: ', num2str(iter) ,', Smooth Loss: ' num2str(smooth_loss)] )
        end
        if(mod(iter, 10000) == 0)
            synth = rnnet.synthSeq(hprev, X(:, 1), 200);
            text=values(ind_to_char, num2cell(vec2ind(synth)));
            result.text{end+1} = cell2mat(text);
            disp(['Iteration: ', num2str(iter), ', Text: ' cell2mat(text)] )
        end
        if smooth_loss < min_loss 
            min_loss = smooth_loss;
            result.loss{end+1} = [epoch, iter, smooth_loss];
            disp(['Iteration: ', num2str(iter) ,', Smooth Loss: ' num2str(smooth_loss)] )
        end
        
        %Prepare for next iteration
        hprev = rnnet.h(:,end);
        prev_loss = smooth_loss;
    end
    
    %Backup the results
    floss = fopen('loss.txt','wt');
    fprintf(floss, '%3i, %10i, %12.8f \r', cell2mat(result.loss));
    fclose(floss);
    
    ftext = fopen('text.txt','wt');
    fprintf(ftext, '%s END \r\r', cell2mat(result.text));
    fclose(ftext);
    
    save 'result.mat' result;
end
%%
% Random search hyper params for either Adagrad or RMSprop
e_min = log10( 0.037); e_max = log10(0.053);
gamma_min = log10(0.9); gamma_max = log10(0.999);
results = cell(50,1);
characters = 200000;
parfor run = 1:50
    disp(['Run: ' num2str(run) ]);
    loss_val = [];
    rnnet = rnn(m,K,sig);
    et = e_min + (e_max - e_min)*rand(1, 1);
    eta = 10^et;
    g = gamma_min + (gamma_max - gamma_min)*rand(1, 1);
    gamma = 10^g;
    for e = 1:seq_length:characters
        if e>characters-seq_length-1
            break;
        end
        X_chars = book_data(e:e+seq_length-1);
        Y_chars = book_data(e+1:e+seq_length);
        X = full(ind2vec(cell2mat(values(char_to_ind, num2cell(char(X_chars)))),K));
        Y = full(ind2vec(cell2mat(values(char_to_ind, num2cell(char(Y_chars)))),K));

        rnnet = rnnet.forwardPass(zeros(m,1), X);
        rnnet = rnnet.backwardPass(X, Y);
        rnnet = rnnet.update(eta);
        %rnnet = rnnet.updateRMS(eta, gamma);
        loss = rnnet.computeCost(Y);
        if (e == 1) 
            smooth_loss = loss;
        end
        smooth_loss = .999* smooth_loss + .001 * loss;
        loss_val =  smooth_loss
    end
%     results{run} = [eta, gamma, loss_val];
    results{run} = [eta, loss_val];
end
resultsSorted = sortrows(cell2mat(results), size(cell2mat(results),2))

%%
%Plot smooth loss evolution
plotdata = reshape(cell2mat(result.loss), 3, []);
figure;
plot(plotdata(2, :), plotdata(3, :))
str=sprintf('Smooth loss evolution for 10 epochs with eta = %f', eta);
title(str);
for i = 44000:44000:plotdata(2,end)
    line([i i], get(gca, 'ylim'), 'Color', 'r')
end
%line([44000 44000], get(gca, 'ylim'))
xlabel('Update step');
ylabel('Smooth loss');

%%END OF MAIN-SCRIPT
%This class contains all of my RNN functions
classdef rnn
    properties
        K; m;
        param;
        grad;
        moment;
        rawmoment;
        a; h; o; P;
    end
    methods
        %Constructor
        function obj = rnn(m, K, sig)
            if nargin == 1
                obj.param = m;
                obj.K = size(obj.param.V,1);
                obj.m = size(obj.param.V,2);
            elseif nargin > 1
                obj.param.W = randn(m, m)*sig;
                obj.grad.W = zeros(m, m);
                obj.moment.W = zeros(m, m);
                obj.rawmoment.W = zeros(m, m);
                
                obj.param.U = randn(m, K)*sig;
                obj.grad.U = zeros(m, K);
                obj.moment.U = zeros(m, K);
                obj.rawmoment.U = zeros(m, K);
                
                obj.param.b = zeros(m,1);
                obj.grad.b = zeros(m,1);
                obj.moment.b = zeros(m, 1);
                obj.rawmoment.b = zeros(m, 1);
                
                obj.param.V = randn(K, m)*sig;
                obj.grad.V = zeros(K, m);
                obj.moment.V = zeros(K, m);
                obj.rawmoment.V = zeros(K, m);
                
                obj.param.c = zeros(K,1);
                obj.grad.c = zeros(K,1);
                obj.moment.c = zeros(K, 1);
                obj.rawmoment.c = zeros(K, 1);
                
                obj.K = K;
                obj.m = m;
            end
        end
        
        %Compute cost
        function cost = computeCost(obj, Y)
            cost = -sum(log(diag(Y'*obj.P)));
        end
        
        %Calculates the difference between 2 gradient matrices. Used to confirm
        %analytical gradients
        function error = relErrOfGrads(obj, grad_num)
            for f = fieldnames(grad_num)'
                error.(f{1}) = sum(sum(abs(obj.grad.(f{1}) - grad_num.(f{1}))))%/numel(grad_num.(f{1}));
            end
        end
        
        %Forward Pass
        function obj = forwardPass(obj, h0, X)
            n = size(X, 2);
            obj.a = zeros(obj.m,n);
            %Will contain h0 at first place
            obj.h = zeros(obj.m,n+1);
            obj.h(:,1) = h0;
            obj.o = zeros(obj.K,n);
            obj.P = zeros(obj.K,n);
            for t = 1:n
                obj.a(:,t) = obj.param.W*obj.h(:,t) + obj.param.U*X(:,t) + obj.param.b;
                obj.h(:,t+1) = tanh(obj.a(:,t));
                obj.o(:,t) = obj.param.V*obj.h(:,t+1) + obj.param.c;
                obj.P(:,t) = softmax(obj.o(:,t));
            end
        end
        %Backwards pass
        function obj = backwardPass(obj, X, Y)
            obj.grad.W = zeros(obj.m, obj.m);
            obj.grad.U = zeros(obj.m, obj.K);
            obj.grad.b = zeros(obj.m,1);
            obj.grad.V = zeros(obj.K, obj.m);             
            obj.grad.c = zeros(obj.K,1);
            for t = size(Y,2):-1:1
                dp = -Y(:,t)'/(Y(:,t)'*obj.P(:,t));
                do = dp * (diag(obj.P(:,t))-obj.P(:,t)*obj.P(:,t)');
                
                obj.grad.c = obj.grad.c + do';
                obj.grad.V = obj.grad.V + do' * obj.h(:,t + 1)';
                
                if t == size(Y,2)
                    dh = do * obj.param.V;
                else    
                    dh = do * obj.param.V + prevda * obj.param.W;
                end
                da = dh * diag (1-tanh(obj.a(:,t)).^2);
                prevda = da;

                obj.grad.W = obj.grad.W + da' * obj.h(:,t)';
                obj.grad.U = obj.grad.U + da' * X(:,t)';
                obj.grad.b = obj.grad.b + da';
            end
            %Clip gradients
            for f = fieldnames(obj.grad)'
                obj.grad.(f{1}) = max(min(obj.grad.(f{1}), 5), -5);
            end
        end
        %Adagrad update
        function obj = update(obj, eta)
            EPS = 1e-8;
            for f = fieldnames(obj.param)'
                obj.moment.(f{1}) = obj.moment.(f{1}) + obj.grad.(f{1}).^2;
                obj.param.(f{1}) =  obj.param.(f{1}) -  (eta * obj.grad.(f{1})) ./ sqrt(obj.moment.(f{1}) + EPS);
             end
        end
        %RMS update
        function obj = updateRMS(obj, eta, gamma)
            EPS = 1e-8;
            for f = fieldnames(obj.param)'
                obj.moment.(f{1}) = gamma*obj.moment.(f{1}) + (1-gamma)*obj.grad.(f{1}).^2;
                obj.param.(f{1}) =  obj.param.(f{1}) -  (eta * obj.grad.(f{1}))./sqrt(obj.moment.(f{1}) + EPS);
             end
        end
        %ADAM update
        function obj = updateADAM(obj, eta, beta1, beta2)
            EPS = 1e-8;
            for f = fieldnames(obj.param)'
                obj.moment.(f{1}) = beta1*obj.moment.(f{1}) + (1-beta1)*obj.grad.(f{1});
                obj.rawmoment.(f{1}) = beta2*obj.rawmoment.(f{1}) + (1-beta2)*(obj.grad.(f{1}).^2);
                obj.param.(f{1}) =  obj.param.(f{1}) -  (eta * obj.moment.(f{1}))./(sqrt(obj.rawmoment.(f{1}))+EPS);
             end
        end
        %Synthesize sequences
        function Y = synthSeq(obj, h0, x, n)
            tempa = zeros(obj.m,n);
            %Will contain h0 at first place
            temph = zeros(obj.m,n+1);
            temph(:,1) = h0;
            tempo = zeros(obj.K,n);
            tempp = zeros(obj.K,n);
            Y = zeros(obj.K,n);
            for t = 1:n
                Y(:,t) = x;
                tempa(:,t) = obj.param.W*temph(:,t) + obj.param.U*x + diag(obj.param.b)*ones(size(obj.param.U,1),1);
                temph(:,t+1) = tanh(tempa(:,t));
                tempo(:,t) = obj.param.V*temph(:,t+1) + obj.param.c;
                tempp(:,t) = softmax(tempo(:,t));
                cp = cumsum(tempp(:,t));
                a = rand;
                ixs = find(cp-a >0);
                ii = ixs(1);
                x = [1:obj.K]'==ii;
            end
        end
    end
end

%Calculates numerical grads
function num_grads = ComputeGradsNum(X, Y, RNN, h)

for f = fieldnames(RNN)'
    disp('Computing numerical gradient for')
    disp(['Field name: ' f{1} ]);
    num_grads.(f{1}) = ComputeGradNum(X, Y, f{1}, RNN, h);
end

function grad = ComputeGradNum(X, Y, f, RNN, h)

n = numel(RNN.(f));
grad = zeros(size(RNN.(f)));
hprev = zeros(size(RNN.W, 1), 1);
for i=1:n
    RNN_try = RNN;
    RNN_try.(f)(i) = RNN.(f)(i) - h;
    
    test = rnn(RNN_try);
    test = test.forwardPass(zeros(test.m,1), X);
    %l1 = ComputeLoss(X, Y, RNN_try, hprev);
    l1 =-sum(log(diag(Y'*test.P)));
    
    RNN_try.(f)(i) = RNN.(f)(i) + h;
    
    test = rnn(RNN_try);
    test = test.forwardPass(zeros(test.m,1), X);
    %l2 = ComputeLoss(X, Y, RNN_try, hprev);
    l2 =-sum(log(diag(Y'*test.P)));
    grad(i) = (l2-l1)/(2*h);
end





