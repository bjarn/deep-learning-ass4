function loss = ComputeLoss( X, Y, RNN_try, hprev )
%COMPUTELOSS Summary of this function goes here
%   Detailed explanation goes here
    for t = 1:n
        a(:,t) = obj.param.W*obj.h(:,t) + obj.param.U*X(:,t) + diag(obj.param.b)*ones(size(obj.param.U,1),1);
        obj.h(:,t+1) = tanh(obj.a(:,t));
        obj.o(:,t) = obj.param.V*obj.h(:,t+1) + obj.param.c;
        obj.P(:,t) = softmax(obj.o(:,t));
    end
    loss = -sum(log(diag(Y'*obj.P)));
end

