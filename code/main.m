%%
%Load data
format long;
book_fname = 'data/goblet_book.txt';
fid = fopen(book_fname,'r');
book_data = fscanf(fid,'%c');
fclose(fid);

%Process data
book_chars = unique(char(book_data));

chars =num2cell(char(book_chars));
indexes = 1:size(book_chars,2);
char_to_ind = containers.Map(chars,indexes);
ind_to_char = containers.Map(indexes,chars);
%%
%Initialize params for RNN
%Hidden nodes
m=100;
seq_length=25;
K=size(book_chars,2);
sig = 0.01;


%%
rnnet = rnn(m,K,sig);
synth = rnnet.synthSeq(zeros(m,1), full(ind2vec(20,K)), 200);

cell2mat(values(ind_to_char, num2cell(vec2ind(synth))))
%%
%Generate 1000 char sequence
synth = rnnet.synthSeq(hprev, full(ind2vec(cell2mat(values(char_to_ind, num2cell(char(book_chars(:, 32))))),84)), 1000);
text=values(ind_to_char, num2cell(vec2ind(synth)));
cell2mat(text)
%%
%Check gradients
X_chars = book_data(1:seq_length);
Y_chars = book_data(2:seq_length+1);
X = full(ind2vec(cell2mat(values(char_to_ind, num2cell(char(X_chars)))),K));
Y = full(ind2vec(cell2mat(values(char_to_ind, num2cell(char(Y_chars)))),K));
rnnet = rnnet.forwardPass(zeros(m,1), X);
cost = rnnet.computeCost(Y);
rnnet = rnnet.backwardPass(X, Y);
num_grads = ComputeGradsNum(X, Y, rnnet.param, 1e-4);
relErr = rnnet.relErrOfGrads(num_grads)
%%
% Random search hyper params for either Adagrad or RMSprop
e_min = log10( 0.037); e_max = log10(0.053);
gamma_min = log10(0.9); gamma_max = log10(0.999);
results = cell(50,1);
characters = 200000;
parfor run = 1:50
    disp(['Run: ' num2str(run) ]);
    loss_val = [];
    rnnet = rnn(m,K,sig);
    et = e_min + (e_max - e_min)*rand(1, 1);
    eta = 10^et;
    g = gamma_min + (gamma_max - gamma_min)*rand(1, 1);
    gamma = 10^g;
    for e = 1:seq_length:characters
        if e>characters-seq_length-1
            break;
        end
        X_chars = book_data(e:e+seq_length-1);
        Y_chars = book_data(e+1:e+seq_length);
        X = full(ind2vec(cell2mat(values(char_to_ind, num2cell(char(X_chars)))),K));
        Y = full(ind2vec(cell2mat(values(char_to_ind, num2cell(char(Y_chars)))),K));

        rnnet = rnnet.forwardPass(zeros(m,1), X);
        rnnet = rnnet.backwardPass(X, Y);
        rnnet = rnnet.update(eta);
        %rnnet = rnnet.updateRMS(eta, gamma);
        loss = rnnet.computeCost(Y);
        if (e == 1) 
            smooth_loss = loss;
        end
        smooth_loss = .999* smooth_loss + .001 * loss;
        loss_val =  smooth_loss
    end
%     results{run} = [eta, gamma, loss_val];
    results{run} = [eta, loss_val];
end
resultsSorted = sortrows(cell2mat(results), size(cell2mat(results),2))

%%
%Train network

n_epoch = 10;
result.loss = {};
result.text = {};
%For Adagrad
% eta = 0.042080200187300;

%For RMSprop
% eta = 0.003809129186068;
% gamma = 0.989664439408021;

%For ADAM
eta = 0.001;
beta1 = 0.9;
beta2 = 0.999;

%kinda cheating
min_loss = 45;
rnnet = rnn(m,K,sig);
prev_loss = 0;
for epoch = 1:n_epoch
    hprev = zeros(m,1);
    disp(['Epoch: ' num2str(epoch) ]);
    for e = 1:seq_length:size(book_data,2)
        if e>length(book_data)-seq_length
            break;
        end
        X_chars = book_data(e:e+seq_length-1);
        Y_chars = book_data(e+1:e+seq_length);
        X = full(ind2vec(cell2mat(values(char_to_ind, num2cell(char(X_chars)))),K));
        Y = full(ind2vec(cell2mat(values(char_to_ind, num2cell(char(Y_chars)))),K));
        
        rnnet = rnnet.forwardPass(zeros(m,1), X);
        rnnet = rnnet.backwardPass(X, Y);
%         %Adagrad
%         rnnet = rnnet.update(eta);
%         %RMS
%         rnnet = rnnet.updateRMS(eta, gamma);
%         %ADAM
        rnnet = rnnet.updateADAM(eta, beta1, beta2);
        
        loss = rnnet.computeCost(Y);
        if (e == 1) && (epoch == 1)
            smooth_loss = loss;
        end
        smooth_loss = .999* smooth_loss + .001 * loss;
        
        iter = ceil((size(book_data,2)*(epoch-1)+e)/seq_length);
        if(mod(iter, 1000) == 0)
            result.loss{end+1} = [epoch, iter, smooth_loss];
            disp(['Iteration: ', num2str(iter) ,', Smooth Loss: ' num2str(smooth_loss)] )
        end
        if(mod(iter, 10000) == 0)
            synth = rnnet.synthSeq(hprev, X(:, 1), 200);
            text=values(ind_to_char, num2cell(vec2ind(synth)));
            result.text{end+1} = cell2mat(text);
            disp(['Iteration: ', num2str(iter), ', Text: ' cell2mat(text)] )
        end
        
        if smooth_loss < min_loss 
            min_loss = smooth_loss;
            result.loss{end+1} = [epoch, iter, smooth_loss];
            disp(['Iteration: ', num2str(iter) ,', Smooth Loss: ' num2str(smooth_loss)] )
            save 'minlossrnn.mat' rnnet;
            save 'minlosshprev.mat' hprev;
        end
        
        hprev = rnnet.h(:,end);
        prev_loss = smooth_loss;
    end
    floss = fopen('loss.txt','wt');
    fprintf(floss, '%3i, %10i, %12.8f \r', cell2mat(result.loss));
    fclose(floss);
    
    ftext = fopen('text.txt','wt');
    fprintf(ftext, '%s END \r\r', cell2mat(result.text));
    fclose(ftext);
    
    save 'result.mat' result;
end
%%
%Plot smooth loss evolution
plotdata = reshape(cell2mat(result.loss), 3, []);
figure;
plot(plotdata(2, :), plotdata(3, :))
%axis([1 n_epochs 1.6 2.3])
str=sprintf('Smooth loss evolution for 10 epochs with eta = %f', eta);
title(str);
for i = 44000:44000:plotdata(2,end)
    line([i i], get(gca, 'ylim'), 'Color', 'r')
end
%line([44000 44000], get(gca, 'ylim'))
xlabel('Update step');
ylabel('Smooth loss');
%%
%
ftext = fopen('text.txt','w');
for i = 1:size(testr,2)
    fprintf(ftext, '%s END \r\r', testr{i});
end
fclose(ftext);
